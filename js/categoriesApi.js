
function createElem(type, parent) {
    var name = document.createElement(type);
    parent.appendChild(name);
    return name;
}
function createElements(x,cat){
    
    var body = document.querySelector("#elements");
    body.innerHTML = "";
    for(let i=0;i<x.length;i++){
        if(cat == x[i][2] ||!cat){
        
        let curr2 = createElem("img",body);
        curr2.src = x[i][1];
        curr2.width="250";
        let curr = createElem("div",body);
        curr.innerHTML = x[i][0];
        }
    }
}

$.ajax({
    url: "../php/products.php",
    success: function (data, textStatus, jsXHR) {
        console.log(data);
        var x = JSON.parse(data);
        createElements(x,0);
        for(let i=0;i<5;i++) {
            let curr = document.querySelector("#cat-"+i);
            console.log(curr);

            curr.onclick = function(){
                console.log(i);
                createElements(x,i);
            }
        }  
    }
});

